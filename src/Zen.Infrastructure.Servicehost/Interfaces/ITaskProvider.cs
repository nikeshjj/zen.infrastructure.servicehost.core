namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    internal interface ITaskProvider
    {
        ITaskInfo GetTaskInfo(string taskId);
        IScheduledTaskFactory GetTaskFactoryByTaskId(string taskId);
    }
}