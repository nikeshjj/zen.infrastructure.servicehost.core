using System.Collections.Generic;
using Zen.Infrastructure.ServiceHost.Contexts;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    internal interface IActiveContextProvider
    {
        bool TryAdd(string taskId, ActiveTaskContext context);
        bool TryGetValue(string taskId, out ActiveTaskContext context);
        bool TryRemove(string taskId, out ActiveTaskContext context);
        IEnumerable<ActiveTaskContext> GetRunningContexts();
        bool Any();
    }
}