namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IQueueRouterConfig
    {
        string QueueRouterId { get; }
        string QueueName { get; }
    }
}