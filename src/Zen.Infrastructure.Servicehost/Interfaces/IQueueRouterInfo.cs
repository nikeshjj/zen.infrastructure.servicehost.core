namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IQueueRouterInfo
    {
        string QueueRouterId { get; }
    }
}