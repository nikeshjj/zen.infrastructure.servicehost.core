﻿using System.Collections.Generic;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IQueueMessage
    {
        string MessageId { get; }
        IDictionary<string, object> Properties { get; }
        string Label { get; }
        T GetBody<T>();
    }
}