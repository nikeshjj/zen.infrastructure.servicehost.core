namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IQueueRouter
    {
        bool IsSatisfiedBy(IQueueRouterInfo info);
        IQueueMessageHandler GetHandler(IQueueMessageInfo info, ITenantConfigurationInfo context);
    }
}