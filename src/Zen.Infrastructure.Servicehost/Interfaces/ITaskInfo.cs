namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface ITaskInfo
    {
        string TaskId { get; }
    }
}