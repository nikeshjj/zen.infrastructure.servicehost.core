using System;
using System.Configuration;
using Zen.Infrastructure.Logging.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface ITenantConfiguration
    {
        void SetLogger(ILogger logger);
        void SetDbConnectionString(string connectionString);
        void SetDbConnectionString(Func<IConfigManager, string> configManager);
        void SetEsbConnectionString(string connectionString);
        void SetEsbConnectionString(Func<IConfigManager, string> configManager);
    }

    public interface IConfigManager
    {
        string ConnectionString(string name);
    }

    public class ConfigManager : IConfigManager
    {
        public string ConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}