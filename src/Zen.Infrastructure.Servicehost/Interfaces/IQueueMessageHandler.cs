﻿using System.Threading;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IQueueMessageHandler
    {
        void Handle(IQueueMessage message, CancellationToken token);
    }
}