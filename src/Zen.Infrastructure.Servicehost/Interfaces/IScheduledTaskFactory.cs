﻿namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IScheduledTaskFactory
    {
        bool IsSatisfiedBy(ITaskInfo info);
        IScheduledTask GetTask(ITaskInfo info, ITenantConfigurationInfo context);
    }
}