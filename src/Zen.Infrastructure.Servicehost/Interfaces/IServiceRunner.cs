namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IServiceRunner
    {
        void OnStart(string[] args);
        void OnStop();
    }
}