using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    internal interface IQueueMessageProvider
    {
        string[] GetQueueRouterIds();
        IQueueSubscriber GetQueueClient(string queueRouterId);
    }
}