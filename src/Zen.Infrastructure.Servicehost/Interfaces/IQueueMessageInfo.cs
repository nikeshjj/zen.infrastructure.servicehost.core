namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IQueueMessageInfo : IQueueMessage
    {
        string QueueRouterId { get; }
    }
}