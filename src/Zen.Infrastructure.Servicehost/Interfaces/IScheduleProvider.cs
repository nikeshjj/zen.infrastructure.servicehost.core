using System;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    internal interface IScheduleProvider
    {
        string[] IdentifyTaskToExecute(DateTime now);
        void PopTaskFromConsideration(string taskId);
        void PushTaskForConsideration(string taskId);
        int GetMaxRetryAttempts(string taskId);
    }
}