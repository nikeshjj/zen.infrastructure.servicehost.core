﻿using System.Threading;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IScheduledTask
    {
        void Execute(CancellationToken token);
    }
}