using System;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface ITaskSchedule
    {
        /// <summary>
        /// The identifier of the Task
        /// </summary>
        string TaskId { get; }

        /// <summary>
        /// Please the schedule you would like to run your task
        /// using the CronTab paradigm.
        /// </summary>
        /// <example>
        /// * * * * *
        /// - - - - -
        /// | | | | |
        /// | | | | +----- day of week (0 - 6) (Sunday=0)
        /// | | | +------- month (1 - 12)
        /// | | +--------- day of month (1 - 31)
        /// | +----------- hour (0 - 23)
        /// +------------- min (0 - 59)
        /// </example>
        /// <remarks>
        /// You can do some research on how to schedule your task via
        /// the documentation on the following links:
        /// 
        /// http://kvz.io/blog/2007/07/29/schedule-tasks-on-linux-using-crontab/ 
        /// </remarks>
        string CronSchedule { get; }

        /// <summary>
        /// The maximum retry attempts for the task
        /// </summary>
        int MaxRetryAttempts { get; }

        /// <summary>
        /// Timezone info object for this task
        /// </summary>
        TimeZoneInfo TimezoneInfoInstance { get; }
    }
}