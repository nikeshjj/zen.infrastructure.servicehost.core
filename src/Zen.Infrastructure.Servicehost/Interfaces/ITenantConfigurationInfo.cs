using Zen.Infrastructure.Logging.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface ITenantConfigurationInfo
    {
        ILogger Logger { get; }
        string DbConnectionString { get; }
        string EsbConnectionString { get; }
    }
}