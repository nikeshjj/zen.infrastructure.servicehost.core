using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    internal interface IQueueRouterProvider
    {
        IQueueMessageInfo GetQueueMessageInfo(string queueRouterId, IMessage message);
        IQueueRouter GetQueueRouterByQueueRouterInfo(string queueRouterId);
    }
}