namespace Zen.Infrastructure.ServiceHost.Interfaces
{
    public interface IBootstrapConfiguration
    {
        /// <summary>
        /// The schedule you would like to run your task
        /// uses the CronTab paradigm.
        /// </summary>
        /// <example>
        /// * * * * *
        /// - - - - -
        /// | | | | |
        /// | | | | +----- day of week (0 - 6) (Sunday=0)
        /// | | | +------- month (1 - 12)
        /// | | +--------- day of month (1 - 31)
        /// | +----------- hour (0 - 23)
        /// +------------- min (0 - 59)
        /// </example>
        /// <remarks>
        /// You can do some research on how to schedule your task via
        /// the documentation on the following links:
        /// 
        /// http://kvz.io/blog/2007/07/29/schedule-tasks-on-linux-using-crontab/ 
        /// </remarks>
        /// <param name="taskId">The task identifier</param>
        /// <param name="cronSchedule">See example for assistance</param>
        /// <param name="maxRetryAttempts">The max amount of retries</param>
        /// <returns></returns>
        IBootstrapConfiguration AddScheduledTask(string taskId, string cronSchedule, int maxRetryAttempts);
        
        /// <summary>
        /// the timezoneName specifies the name timezone for given task
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="cronSchedule"></param>
        /// <param name="maxRetryAttempts"></param>
        /// <param name="timezoneName"></param>
        /// <returns></returns>
        IBootstrapConfiguration AddScheduledTask(string taskId, string cronSchedule, int maxRetryAttempts, string timezoneName);

        /// <summary>
        /// The queue router that you would like to listen to.
        /// </summary>
        /// <param name="queueRouterId">The queue router identifier</param>
        /// <param name="queueName">The queue name</param>
        /// <returns></returns>
        IBootstrapConfiguration AddQueueRouter(string queueRouterId, string queueName);
    }
}