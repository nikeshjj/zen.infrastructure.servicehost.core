using System.Data;
using System.Linq;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Providers
{
    public class QueueMessageProvider : IQueueMessageProvider
    {
        private readonly IQueueRouterConfig[] _listeners;
        private readonly IQueueProvider _provider;

        public QueueMessageProvider(IQueueRouterConfig[] listeners, IQueueProvider provider)
        {
            _listeners = listeners;
            _provider = provider;
        }

        public string[] GetQueueRouterIds()
        {
            return _listeners
                .Select(x => x.QueueRouterId)
                .ToArray();
        }

        public IQueueSubscriber GetQueueClient(string queueRouterId)
        {
            var listener = _listeners
                .SingleOrDefault(x => x.QueueRouterId == queueRouterId);

            if (listener == null)
                throw new NoNullAllowedException("There was no listener available for the taskId.");

            return _provider.GetSubscriber(listener.QueueName);
        }
    }
}