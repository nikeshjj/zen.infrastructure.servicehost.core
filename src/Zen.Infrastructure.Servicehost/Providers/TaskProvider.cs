﻿using System.Linq;
using Zen.Infrastructure.ServiceHost.Interfaces;
using Zen.Infrastructure.ServiceHost.Models;

namespace Zen.Infrastructure.ServiceHost.Providers
{
    public class TaskProvider : ITaskProvider
    {
        private readonly IScheduledTaskFactory[] _factories;

        public TaskProvider(IScheduledTaskFactory[] factories)
        {
            _factories = factories;
        }

        public ITaskInfo GetTaskInfo(string taskId)
        {
            return new TaskInfo
            {
                TaskId = taskId
            };
        }

        public IScheduledTaskFactory GetTaskFactoryByTaskId(string taskId)
        {
            var info = GetTaskInfo(taskId);

            return _factories
                .FirstOrDefault(x => x.IsSatisfiedBy(info));
        }
    }
}
