using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Zen.Infrastructure.ServiceHost.Contexts;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Providers
{
    public class ActiveContextProvider : IActiveContextProvider
    {
        private readonly ConcurrentDictionary<string, ActiveTaskContext> _contexts;

        public ActiveContextProvider()
        {
            _contexts = new ConcurrentDictionary<string, ActiveTaskContext>();
        }

        public bool TryAdd(string taskId, ActiveTaskContext context)
        {
            return _contexts.TryAdd(taskId, context);
        }

        public bool TryGetValue(string taskId, out ActiveTaskContext context)
        {
            return _contexts.TryGetValue(taskId, out context);
        }

        public bool TryRemove(string taskId, out ActiveTaskContext context)
        {
            return _contexts.TryRemove(taskId, out context);
        }

        public IEnumerable<ActiveTaskContext> GetRunningContexts()
        {
            return _contexts
                .Values
                .Where(x => x.IsRunning)
                .ToArray();
        }

        public bool Any()
        {
            return _contexts.Any();
        }
    }
}