using System.Linq;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;
using Zen.Infrastructure.ServiceHost.Models;

namespace Zen.Infrastructure.ServiceHost.Providers
{
    public class QueueRouterProvider : IQueueRouterProvider
    {
        private readonly IQueueRouter[] _routers;

        public QueueRouterProvider(IQueueRouter[] routers)
        {
            _routers = routers;
        }

        public IQueueMessageInfo GetQueueMessageInfo(string queueRouterId, IMessage message)
        {
            return new QueueMessageInfo(queueRouterId, message);
        }

        public IQueueRouter GetQueueRouterByQueueRouterInfo(string queueRouterId)
        {
            var info = new QueueRouterInfo
            {
                QueueRouterId = queueRouterId
            };

            return _routers
                .FirstOrDefault(x => x.IsSatisfiedBy(info));
        }
    }
}