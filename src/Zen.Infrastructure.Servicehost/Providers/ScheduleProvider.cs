using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using NCrontab;
using Zen.Infrastructure.Logging;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Providers
{
    public class ScheduleProvider : IScheduleProvider
    {
        private readonly ILogger _logger;
        private readonly ITaskSchedule[] _taskSchedules;
        private readonly IDictionary<string, DateTime> _schedules;

        public ScheduleProvider(ILogger logger, ITaskSchedule[] taskSchedules)
        {
            _logger = logger;
            _taskSchedules = taskSchedules;

            _schedules = new ConcurrentDictionary<string, DateTime>();
            
            Initialize();
        }

        private void Initialize()
        {
            foreach (var taskSchedule in _taskSchedules)
                PushTaskForConsideration(taskSchedule);
        }

        public string[] IdentifyTaskToExecute(DateTime date)
        {
            return _schedules.Where(x => x.Value <= date)
                .Select(x => x.Key)
                .ToArray();
        }

        public void PopTaskFromConsideration(string taskId)
        {
            if (!_schedules.ContainsKey(taskId))
                return;

            _logger.Debug("Removing \"{taskId}\" from consideration.", taskId);

            lock (_schedules)
            {
                _schedules.Remove(taskId);
            }
        }

        public void PushTaskForConsideration(string taskId)
        {
            var taskSchedule = _taskSchedules
                .FirstOrDefault(x => x.TaskId == taskId);

            PushTaskForConsideration(taskSchedule);
        }

        public int GetMaxRetryAttempts(string taskId)
        {
            var taskSchedule = _taskSchedules
                .FirstOrDefault(x => x.TaskId == taskId);

            if (taskSchedule == null)
                return 0;

            return taskSchedule.MaxRetryAttempts;
        }

        private void PushTaskForConsideration(ITaskSchedule taskSchedule)
        {
            if (_schedules.ContainsKey(taskSchedule.TaskId)) 
                return;

            var utcValue = new DateTime(DateTime.UtcNow.Ticks, DateTimeKind.Unspecified);
            var tmzTime = TimeZoneInfo.ConvertTime(utcValue, TimeZoneInfo.Utc, taskSchedule.TimezoneInfoInstance);

            var schedule = CrontabSchedule.Parse(taskSchedule.CronSchedule);
            var nextExecutionForTimezone = schedule.GetNextOccurrence(tmzTime);  //get timezone specific scheduling info

            var tmzkindValue = new DateTime(nextExecutionForTimezone.Ticks, DateTimeKind.Unspecified);
            var nextExecutionAsPerServerTime = TimeZoneInfo.ConvertTime(tmzkindValue, taskSchedule.TimezoneInfoInstance, TimeZoneInfo.Local);

            _schedules[taskSchedule.TaskId] = nextExecutionAsPerServerTime;


            var localServerTime = DateTime.Now;
            var diff = nextExecutionAsPerServerTime.Subtract(localServerTime).ToString("g");
            _logger.Information("Added \"{taskId}\" for consideration. \nNext execution is: \"{nextExecutionAsPerServerTime}\" \nCurrent Utc time is:  \"{currentUTCDateTime}\" \nCurrent time in busiess timezone is:  \"{currentTimezoneDateTime}\" \nLocal Server time is: \"{localServerDateTime}\". \nWait time till execution: {waitTimeTillNextExecution}"
                , taskSchedule.TaskId
                , nextExecutionAsPerServerTime, utcValue, tmzTime, localServerTime, diff);
        }
    }
}