using System.Collections.Generic;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Providers
{
    public class QueueMessageInfo : IQueueMessageInfo
    {
        private readonly string _queueRouterId;
        private readonly IMessage _message;

        public QueueMessageInfo(string queueRouterId, IMessage message)
        {
            _queueRouterId = queueRouterId;
            _message = message;
        }

        public string QueueRouterId
        {
            get { return _queueRouterId; }
        }

        public string MessageId
        {
            get { return _message.MessageId; }
        }

        public IDictionary<string, object> Properties
        {
            get { return _message.Properties; }
        }

        public string Label
        {
            get { return _message.Label; }
        }

        public T GetBody<T>()
        {
            return _message.GetBody<T>();
        }
    }
}