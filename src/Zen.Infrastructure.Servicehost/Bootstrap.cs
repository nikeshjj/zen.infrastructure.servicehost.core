﻿using System;
using Zen.Infrastructure.ServiceHost.Interfaces;
using Zen.Infrastructure.ServiceHost.Runners;

namespace Zen.Infrastructure.ServiceHost
{
    public static class Bootstrap
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(string[] args, Action<ITenantConfiguration> tenantConfig, Action<IBootstrapConfiguration> action)
        {
            var configuration = new BoostrapConfiguration();

            var tenantConfiguration = new TenantConfiguration();

            if (tenantConfig != null)
                tenantConfig(tenantConfiguration);

            if (action != null)
                action(configuration);

            if (Environment.UserInteractive)
                ConsoleRunner.Start(args, tenantConfiguration, configuration.Schedules, configuration.QueueRouters);
            else
                ServiceRunner.Start(args, tenantConfiguration, configuration.Schedules, configuration.QueueRouters);
        }
    }
}
