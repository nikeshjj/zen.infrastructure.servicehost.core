using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost
{
    internal class SubscriberController
    {
        private readonly ITenantConfigurationInfo _tenantConfiguration;
        private readonly IQueueMessageProvider _queueMessageProvider;
        private readonly IQueueRouterProvider _queueRouterProvider;
        private readonly List<Task> _tasks;

        public SubscriberController(ITenantConfigurationInfo tenantConfiguration, IQueueMessageProvider queueMessageProvider, IQueueRouterProvider queueRouterProvider)
        {
            _tenantConfiguration = tenantConfiguration;
            _queueMessageProvider = queueMessageProvider;
            _queueRouterProvider = queueRouterProvider;
            _tasks = new List<Task>();
        }

        public void ExecuteThread(CancellationToken token)
        {
            using (var manual = new ManualResetEventSlim())
            {
                foreach (var taskId in _queueMessageProvider.GetQueueRouterIds())
                {
                    Thread.Sleep(250);

                    InitiateSubscriber(taskId, token);

                    try
                    {
                        manual.Wait(TimeSpan.FromSeconds(5), token);
                    }
                    catch (OperationCanceledException)
                    {
                        break;
                    }
                }
            }

            WaitTillCancel(token);
            WaitOnTasks(_tasks.ToArray());
        }

        private void InitiateSubscriber(string queueRouterId, CancellationToken token)
        {
            var task = new Task(() =>
            {
                using (var client = _queueMessageProvider.GetQueueClient(queueRouterId))
                {
                    var router = _queueRouterProvider.GetQueueRouterByQueueRouterInfo(queueRouterId);

                    client.Consume(message =>
                    {
                        _tenantConfiguration.Logger.Debug("Starting to consume message {messageId}.", message.MessageId);

                        try
                        {
                            var queueMessageInfo = _queueRouterProvider.GetQueueMessageInfo(queueRouterId, message);
                            var instance = LoadHandlerInstance(router, queueMessageInfo);
                            instance.Handle(queueMessageInfo, token);

                            message.Complete();
                            _tenantConfiguration.Logger.Debug("Successfully consumed message {messageId}.", message.MessageId);
                        }
                        catch (Exception ex)
                        {
                            IDictionary<string, object> propertiesToModify;
                            if (HandleExceptionAndRetry(queueRouterId, message, ex, out propertiesToModify))
                                message.Abandon(propertiesToModify);
                            else
                                message.DeadLetter("Message Handling Exception", ex.Message);
                        }
                    });

                    _tenantConfiguration.Logger.Debug("Listening to message for queueRouter {queueRouterId}", queueRouterId);

                    WaitTillCancel(token);
                }

                _tenantConfiguration.Logger.Debug("The Queue Router {queueRouterId} task is terminating.", queueRouterId);
            });
            task.ContinueWith(x => ContinueOnComplete(x, queueRouterId), token);
            task.ContinueWith(x => ContinueOnCancellation(queueRouterId), TaskContinuationOptions.OnlyOnCanceled);
            task.ContinueWith(x => ContinueOnFault(x, queueRouterId), TaskContinuationOptions.OnlyOnFaulted);
            
            _tasks.Add(task);
            task.Start();
        }

        private void ContinueOnComplete(Task task, string queueRouterId)
        {
            _tenantConfiguration.Logger.Information("The {queueRouterId} queueRouter is shutting down.", queueRouterId);
            _tasks.Remove(task);
        }

        private void ContinueOnCancellation(string queueRouterId)
        {
            _tenantConfiguration.Logger.Warning("The {queueRouterId} queueRouter has been asked to cancel gracefully.", queueRouterId);
        }

        private void ContinueOnFault(Task task, string queueRouterId)
        {
            _tenantConfiguration.Logger.Error(task.Exception, "An error occurred while listening to queueRouterId {queueRouterId}.", queueRouterId);
        }

        private bool HandleExceptionAndRetry(string queueRouterId, IMessage message, Exception ex, out IDictionary<string, object> propertiesToModify)
        {
            object retryNumber;
            if (message.Properties.TryGetValue("x-retry", out retryNumber))
                retryNumber = (int)retryNumber + 1;
            else
                retryNumber = 1;

            propertiesToModify = new Dictionary<string, object>
            {
                {"x-retry", retryNumber}
            };

            if ((int) retryNumber > 5)
            {
                _tenantConfiguration.Logger.Context("queueRouterId", queueRouterId)
                   .Context("action", message.Properties)
                   .Error(ex, "An error occurred while handling the message {messageId} and max retry attempts is over 5.", message.MessageId);
                return false;
            }

            _tenantConfiguration.Logger.Context("queueRouterId", queueRouterId)
                .Context("action", message.Properties)
                .Context("retryNumber", retryNumber)
                .Error(ex, "An error occurred while handling the message {messageId} will retry shortly.", message.MessageId);
            
            return true;
        }

        private IQueueMessageHandler LoadHandlerInstance(IQueueRouter router, IQueueMessageInfo info)
        {
            if (router == null)
            {
                var message = string.Format("There was no factory containing the task definition \"{0}\""
                    , info.QueueRouterId);
                throw new InvalidOperationException(message);
            }
            
            var instance = router.GetHandler(info, _tenantConfiguration);
            if (instance == null)
            {
                var message = string.Format("There was instance returned from factory for task definition \"{0}\""
                    , info.QueueRouterId);
                throw new InvalidOperationException(message);
            }

            return instance;
        }

        private static void WaitTillCancel(CancellationToken token)
        {
            using (var manual = new ManualResetEventSlim())
            {
                while (true)
                {
                    try
                    {
                        manual.Wait(TimeSpan.FromMinutes(1), token);
                    }
                    catch (OperationCanceledException)
                    {
                        break;
                    }
                }
            }
        }

        private void WaitOnTasks(Task[] tasks)
        {
            if (!tasks.Any())
                return;

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ex)
            {
                _tenantConfiguration.Logger.Warning(ex, "Expected error occurred while waiting for \"{count}\" task(s) to complete.",
                    tasks.Length);
            }
            catch (Exception ex)
            {
                _tenantConfiguration.Logger.Error(ex, "Unknown error occurred while waiting for \"{count}\" task(s) to complete.",
                    tasks.Length);
            }
        }
    }
}