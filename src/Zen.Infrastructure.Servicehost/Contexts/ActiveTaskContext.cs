using System;
using System.Threading;
using System.Threading.Tasks;

namespace Zen.Infrastructure.ServiceHost.Contexts
{
    public class ActiveTaskContext : IDisposable
    {
        public string TaskId { get; private set; }
        public bool IsRunning { get; private set; }
        public DateTime StartDate { get; private set; }
        public Task RunningTask { get; private set; }
        public CancellationToken Token { get; private set; }
        public bool WasTaskCancelled { get; set; }

        public static ActiveTaskContext CreateContext(string taskId)
        {
            return new ActiveTaskContext
            {
                TaskId = taskId
            };
        }

        public void SetActiveTask(Task task, CancellationToken token)
        {
            StartDate = DateTime.Now;
            IsRunning = true;
            RunningTask = task;
            Token = token;
        }

        public void Dispose()
        {
            if (RunningTask != null)
                RunningTask.Dispose();
        }
    }
}