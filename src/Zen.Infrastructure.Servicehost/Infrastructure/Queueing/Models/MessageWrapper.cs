using System.Collections.Generic;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Models
{
    public class MessageWrapper : IMessage
    {
        private readonly BrokeredMessage _message;

        public MessageWrapper(BrokeredMessage message)
        {
            _message = message;
        }

        public string CorrelationId
        {
            get { return _message.CorrelationId; }
        }

        public string Label
        {
            get { return _message.Label; }
        }

        public IDictionary<string, object> Properties
        {
            get { return _message.Properties; }
        }

        public string MessageId
        {
            get { return _message.MessageId; }
        }

        public T GetBody<T>()
        {
            var wrapper = _message.GetBody<BodyMessage>();
            return JsonConvert.DeserializeObject<T>(wrapper.Json);
        }

        public void Complete()
        {
            _message.Complete();
        }

        public void DeadLetter(string reason, string description)
        {
            _message.DeadLetter(reason, description);
        }

        public void Abandon(IDictionary<string, object> propertiesToModify)
        {
            _message.Abandon(propertiesToModify);
        }
    }
}
