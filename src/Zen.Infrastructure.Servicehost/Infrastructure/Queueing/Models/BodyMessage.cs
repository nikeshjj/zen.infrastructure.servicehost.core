namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Models
{
    public class BodyMessage
    {
        public string Json { get; set; }
    }
}