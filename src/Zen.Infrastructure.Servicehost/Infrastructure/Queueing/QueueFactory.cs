using System.Collections.Generic;
using System.Linq;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing
{
    public static class QueueFactory
    {
        private static readonly IDictionary<string, QueueProvider> Dictionary = new Dictionary<string, QueueProvider>();

        public static IQueueProvider Create(string connectionString)
        {
            lock (Dictionary)
            {
                if (connectionString == null)
                    return null;

                QueueProvider provider;
                if (Dictionary.TryGetValue(connectionString, out provider) && provider != null && provider.IsActive)
                    return provider;

                provider = new QueueProvider(connectionString);
                provider.Activate();
                Dictionary.Add(connectionString, provider);

                return provider;
            }
        }

        public static void DisposeConnectionPool()
        {
            lock (Dictionary)
            {
                var providers = Dictionary.Values.ToArray();

                foreach (var provider in providers)
                    provider.Dispose();

                Dictionary.Clear();
            }
        }
    }
}