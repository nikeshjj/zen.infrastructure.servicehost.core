using System.Collections.Generic;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces
{
    public interface IMessage
    {
        string CorrelationId { get; }
        string Label { get; }
        IDictionary<string, object> Properties { get; }
        string MessageId { get; }
        T GetBody<T>();
        void Complete();
        void DeadLetter(string reason, string description);
        void Abandon(IDictionary<string, object> propertiesToModify);
    }
}