using System;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces
{
    public interface IQueueProvider : IDisposable
    {
        IQueuePublisher GetPublisher(string path);
        IQueueSubscriber GetSubscriber(string path);
    }
}