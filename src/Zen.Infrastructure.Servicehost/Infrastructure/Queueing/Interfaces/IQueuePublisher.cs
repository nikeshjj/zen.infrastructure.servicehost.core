using System.Collections.Generic;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces
{
    public interface IQueuePublisher
    {
        void Publish(string json);
        void Publish(IDictionary<string, object> properties, string json);
        void Publish<T>(T payload);
        void Publish<T>(IDictionary<string, object> properties, T payload);
    }
}