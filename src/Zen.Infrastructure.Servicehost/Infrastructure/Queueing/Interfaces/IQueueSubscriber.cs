using System;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces
{
    public interface IQueueSubscriber : IDisposable
    {
        void Consume(Action<IMessage> action);
        void Close();
    }
}