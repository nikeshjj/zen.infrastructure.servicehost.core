﻿using System.Collections.Generic;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Models;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing
{
    internal class QueuePublisher : IQueuePublisher
    {
        private readonly QueueProvider _provider;
        private readonly string _path;

        public QueuePublisher(QueueProvider provider, string path)
        {
            _provider = provider;
            _path = path;
        }

        public void Publish(string json)
        {
            Publish(null, json);
        }

        public void Publish(IDictionary<string, object> properties, string json)
        {
            var message = new BrokeredMessage(new BodyMessage
            {
                Json = json
            });

            if (properties != null)
                foreach (var property in properties)
                {
                    message.Properties[property.Key] = property.Value;
                }

            _provider.Client(_path).Send(message);
        }

        public void Publish<T>(T payload)
        {
            Publish(null, payload);
        }

        public void Publish<T>(IDictionary<string, object> properties, T payload)
        {
            var json = JsonConvert.SerializeObject(payload);
            var message = new BrokeredMessage(new BodyMessage
            {
                Json = json
            });

            if (properties != null)
                foreach (var property in properties)
                {
                    message.Properties[property.Key] = property.Value;
                }

            _provider.Client(_path).Send(message);
        }
    }
}
