using System;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Models;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing
{
    internal class QueueSubscriber : IQueueSubscriber
    {
        private readonly QueueProvider _provider;
        private readonly string _path;
        private bool _isClosed;

        public QueueSubscriber(QueueProvider provider, string path)
        {
            _provider = provider;
            _path = path;
        }

        public void Consume(Action<IMessage> action)
        {
            _provider.Client(_path).OnMessage(message =>
            {
                var wrapper = new MessageWrapper(message);
                action(wrapper);
            });
        }

        public void Close()
        {
            lock (_provider)
            {
                if (!_isClosed)
                {
                    _provider.Client(_path).Close();
                }
                _isClosed = true;
            }
        }

        public void Dispose()
        {
            Close();
        }
    }
}