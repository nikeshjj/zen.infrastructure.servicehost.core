using System.Collections.Generic;
using System.Linq;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Infrastructure.Queueing
{
    internal class QueueProvider : IQueueProvider
    {
        private readonly string _connectionString;
        private readonly IDictionary<string, QueueClient> _pool;
        private NamespaceManager _namespaceManager;
        private bool _active;

        internal QueueProvider(string connectionString)
        {
            _connectionString = connectionString;
            _pool = new Dictionary<string, QueueClient>();
        }

        private NamespaceManager Manager
        {
            get {
                return _namespaceManager ??
                       (_namespaceManager = NamespaceManager.CreateFromConnectionString(_connectionString));
            }
        }

        internal QueueClient Client(string path)
        {
            return Client(path, 10);
        }

        internal QueueClient Client(string path, int prefetchCount)
        {
            lock (_pool)
            {
                QueueClient client;
                var token = string.Format("{0}::{1}", path, prefetchCount);

                if (_pool.TryGetValue(token, out client) && client != null && !client.IsClosed)
                    return client;

                if (!Manager.QueueExists(path))
                    Manager.CreateQueue(path);

                client = QueueClient.CreateFromConnectionString(_connectionString, path);
                client.PrefetchCount = prefetchCount;
                _pool[token] = client;

                return client;
            }
        }

        internal bool IsActive
        {
            get { return _active; }
        }

        internal void Activate()
        {
            _active = true;
        }

        public IQueuePublisher GetPublisher(string path)
        {
            return new QueuePublisher(this, path);
        }

        public IQueueSubscriber GetSubscriber(string path)
        {
            return new QueueSubscriber(this, path);
        }

        public void Dispose()
        {
            _active = false;
            lock (_pool)
            {
                var items = _pool.Values.ToArray();
                foreach (var item in items)
                {
                    item.Close();
                }
                _pool.Clear();
            }
        }
    }
}