using System;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Models
{
    public class TaskSchedule : ITaskSchedule
    {
        public string TaskId { get; private set; }
        public string CronSchedule { get; private set; }
        public int MaxRetryAttempts { get; private set; }

        public TimeZoneInfo TimezoneInfoInstance { get; private set; }

        public static ITaskSchedule Create(string taskId, string cronSchedule, int maxRetryAttempts, TimeZoneInfo tmzInfo)
        {
            return new TaskSchedule
            {
                TaskId = taskId,
                CronSchedule = cronSchedule,
                MaxRetryAttempts = maxRetryAttempts,
                TimezoneInfoInstance = tmzInfo ?? TimeZoneInfo.Utc //add timezone - treat default utc
            };
        }
    }
}