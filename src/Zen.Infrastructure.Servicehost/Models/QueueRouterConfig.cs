using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Models
{
    public class QueueRouterConfig : IQueueRouterConfig
    {
        public string QueueRouterId { get; set; }
        public string QueueName { get; set; }

        public static IQueueRouterConfig Create(string queueRouterId, string queueName)
        {
            return new QueueRouterConfig
            {
                QueueRouterId = queueRouterId,
                QueueName = queueName
            };
        }
    }
}