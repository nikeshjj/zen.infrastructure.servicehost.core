using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Models
{
    public class TaskDefinition : ITaskDefinition
    {
        public string TaskId { get; private set; }
        public string CronSchedule { get; private set; }
        public int MaxRetryAttempts { get; private set; }

        public static ITaskDefinition Create(string taskId, string cronSchedule, int maxRetryAttempts)
        {
            return new TaskDefinition
            {
                TaskId = taskId,
                CronSchedule = cronSchedule,
                MaxRetryAttempts = maxRetryAttempts
            };
        }
    }
}