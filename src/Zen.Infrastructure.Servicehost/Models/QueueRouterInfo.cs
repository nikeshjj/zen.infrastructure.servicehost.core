using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Models
{
    public class QueueRouterInfo : IQueueRouterInfo
    {
        public string QueueRouterId { get; set; }
    }
}