﻿using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Models
{
    public class TaskInfo : ITaskInfo
    {
        public string TaskId { get; set; }
    }
}