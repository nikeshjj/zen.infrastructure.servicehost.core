using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing;
using Zen.Infrastructure.ServiceHost.Interfaces;
using Zen.Infrastructure.ServiceHost.Providers;

namespace Zen.Infrastructure.ServiceHost
{
    public class ServiceFactory
    {
        public static ILogger CreateLogger(ITenantConfigurationInfo tenantConfiguration, string logName)
        {
            return tenantConfiguration.Logger;
        }

        internal static IScheduleProvider GetScheduleProvider(ILogger logger, ITaskSchedule[] schedules)
        {
            return new ScheduleProvider(logger, schedules);
        }

        internal static IActiveContextProvider GetActiveContextProviders()
        {
            return new ActiveContextProvider();
        }

        internal static ITaskProvider GetTaskProvider(IScheduledTaskFactory[] factories)
        {
            return new TaskProvider(factories);
        }

        internal static IQueueRouterProvider GetSubscriberProvider(IQueueRouter[] routers)
        {
            return new QueueRouterProvider(routers);
        }

        internal static IQueueMessageProvider GetListenerProvider(ITenantConfigurationInfo tenantConfiguration, ILogger logger, IQueueRouterConfig[] listeners)
        {
            var provider = QueueFactory.Create(tenantConfiguration.EsbConnectionString);
            return new QueueMessageProvider(listeners, provider);
        }
    }
}