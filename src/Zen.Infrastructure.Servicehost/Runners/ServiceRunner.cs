﻿using System.ServiceProcess;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Runners
{
    partial class ServiceRunner : ServiceBase
    {
        private readonly ServiceManager _manager;

        public static void Start(string[] args, ITenantConfigurationInfo tenantConfiguration, ITaskSchedule[] schedules, IQueueRouterConfig[] queueRouters)
        {
            var servicesToRun = new ServiceBase[] { new ServiceRunner(tenantConfiguration, schedules, queueRouters) };
            Run(servicesToRun);
        }

        private ServiceRunner(ITenantConfigurationInfo tenantConfiguration, ITaskSchedule[] schedules, IQueueRouterConfig[] queueRouters)
        {
            InitializeComponent();
            _manager = new ServiceManager(tenantConfiguration, schedules, queueRouters);
        }

        protected override void OnStart(string[] args)
        {
            _manager.OnStart(args);
        }

        protected override void OnStop()
        {
            _manager.OnStop(ExitCode);
        }
    }
}
