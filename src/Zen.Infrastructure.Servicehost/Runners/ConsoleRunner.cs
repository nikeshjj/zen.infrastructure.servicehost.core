using System;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.Runners
{
    public static class ConsoleRunner
    {
        public static void Start(string[] args, ITenantConfigurationInfo tenantConfigInfo, ITaskSchedule[] schedules, IQueueRouterConfig[] queueRouters)
        {
            using (var manager = new ServiceManager(tenantConfigInfo, schedules, queueRouters))
            {
                Console.WriteLine("Starting the service runner now.");

                manager.OnStart(args);

                Console.CancelKeyPress += (o, e) =>
                {
                    Console.WriteLine("Ctrl+C was entered. Shutting down.");

                    manager.OnStop(0);

                    Console.WriteLine("Press any key to close the console.");
                    Console.ReadKey();
                };

                while (true)
                {
                    var key = Console.ReadKey();
                    if (key.Key != ConsoleKey.Q)
                        continue;

                    Console.WriteLine("Q was entered. Shutting down.");

                    manager.OnStop(0);

                    Console.WriteLine("Press any key to close the console.");
                    Console.ReadKey();
                    break;
                }
            }
        }
    }
}