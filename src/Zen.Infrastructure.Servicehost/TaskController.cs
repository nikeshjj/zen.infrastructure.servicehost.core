using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Zen.Infrastructure.ServiceHost.Contexts;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost
{
    internal class TaskController
    {
        private readonly ITenantConfigurationInfo _tenantConfiguration;
        private readonly IScheduleProvider _scheduleProvider;
        private readonly IActiveContextProvider _activeContextProvider;
        private readonly ITaskProvider _taskProvider;

        public TaskController(ITenantConfigurationInfo tenantConfiguration, IScheduleProvider scheduleProvider, IActiveContextProvider activeContextProvider, ITaskProvider taskProvider)
        {
            _tenantConfiguration = tenantConfiguration;
            _scheduleProvider = scheduleProvider;
            _activeContextProvider = activeContextProvider;
            _taskProvider = taskProvider;
        }

        public void ExecuteThread(CancellationToken token)
        {
            using (var manual = new ManualResetEventSlim())
            {
                while (true)
                {
                    try
                    {
                        var ids = _scheduleProvider.IdentifyTaskToExecute(DateTime.Now);
                        _tenantConfiguration.Logger.Debug("Identified \"{count}\" tasks that should run.", ids.Length);

                        foreach (var taskId in ids)
                        {
                            _tenantConfiguration.Logger.Debug("The \"{taskId}\" task should run.", taskId);

                            ActiveTaskContext context;
                            if (_activeContextProvider.TryGetValue(taskId, out context) && context.IsRunning)
                            {
                                _tenantConfiguration.Logger.Warning(
                                    "Task \"{taskId}\" has been scheduled to run but is currently active.",
                                    taskId);

                                _tenantConfiguration.Logger.Information(
                                    "Task \"{taskId}\" has been active since {startDate}.",
                                    taskId, context.StartDate);

                                continue;
                            }

                            ExecuteTask(taskId, token);
                        }

                        try
                        {
                            manual.Wait(TimeSpan.FromSeconds(15), token);
                        }
                        catch (OperationCanceledException)
                        {
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        var strBuilder = new StringBuilder(ex.Message);
                        strBuilder.AppendLine();
                        strBuilder.Append(ex.StackTrace);
                        _tenantConfiguration.Logger.Debug("Exception Occured in Task Controller. Stack trace: \"{error}\"", strBuilder.ToString());
                    }

                }
            }

            var runningTasks = _activeContextProvider.GetRunningContexts()
                .Select(x => x.RunningTask)
                .ToArray();

            if (_activeContextProvider.Any())
                WaitOnTasks(runningTasks);
        }

        private void ExecuteTask(string taskId, CancellationToken token)
        {
            _scheduleProvider.PopTaskFromConsideration(taskId);
            var context = ActiveTaskContext.CreateContext(taskId);
            var maxRetryAttempts = _scheduleProvider.GetMaxRetryAttempts(taskId);

            var task = new Task(() =>
            {
                var instance = LoadInstance(taskId);
                var guid = Guid.NewGuid().ToString();
                _tenantConfiguration.Logger.Information("Execution {_monitoringTaskStatus} for task \"{taskId}\".\r\n{_threadGuid}", "started", taskId, guid);

                var actionExecutedWithoutException = Retry(maxRetryAttempts, token, () =>
                {
                    instance.Execute(context.Token);
                });

                _tenantConfiguration.Logger.Information("Execution {_monitoringTaskStatus} for task \"{taskId}\".\r\n{_threadGuid}",
                    actionExecutedWithoutException ? "done" : "failed", taskId, guid);
            });
            task.ContinueWith(x => ContinueOnComplete(context), TaskContinuationOptions.OnlyOnRanToCompletion);
            task.ContinueWith(x => ContinueOnCancellation(context), TaskContinuationOptions.OnlyOnCanceled);
            task.ContinueWith(x => ContinueOnFault(context), TaskContinuationOptions.OnlyOnFaulted);

            _activeContextProvider.TryAdd(taskId, context);
            context.SetActiveTask(task, token);

            task.Start();
        }

        private bool Retry(int maxAttempts, CancellationToken token, Action action)
        {
            var actionExecutedWithoutException = false;
            using (var manual = new ManualResetEventSlim())
            {
                var attempt = 0;
                while (attempt < maxAttempts)
                {
                    try
                    {
                        action();
                        actionExecutedWithoutException = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        _tenantConfiguration.Logger.Warning("Waiting for \"{waiting}\" seconds before retrying...", 15);

                        try
                        {
                            manual.Wait(TimeSpan.FromSeconds(15), token);
                        }
                        catch (OperationCanceledException)
                        {
                            break;
                        }

                        attempt++;
                        _tenantConfiguration.Logger.Error(ex
                            , "Attempt \"{attempt}\" of \"{maxAttempts}\" failed: \"{errorMessage}\"."
                            , attempt
                            , maxAttempts
                            , ex.Message);
                    }
                }
            }
            return actionExecutedWithoutException;
        }

        private void ContinueOnComplete(ActiveTaskContext context)
        {
            ActiveTaskContext existingContext;
            if (!_activeContextProvider.TryRemove(context.TaskId, out existingContext))
            {
                _tenantConfiguration.Logger.Warning("Unable to remove task \"{taskId}\".", context.TaskId);
                return;
            }

            if (!context.WasTaskCancelled)
                _scheduleProvider.PushTaskForConsideration(context.TaskId);

            context.Dispose();
        }

        private void ContinueOnCancellation(ActiveTaskContext context)
        {
            ActiveTaskContext existingContext;
            if (!_activeContextProvider.TryRemove(context.TaskId, out existingContext))
            {
                _tenantConfiguration.Logger.Warning("Unable to remove task \"{taskId}\".", context.TaskId);
                return;
            }

            _tenantConfiguration.Logger.Warning("Removed task \"{taskId}\" from the context list.", context.TaskId);

            context.Dispose();
        }

        private void ContinueOnFault(ActiveTaskContext context)
        {
            ActiveTaskContext existingContext;
            if (!_activeContextProvider.TryRemove(context.TaskId, out existingContext))
            {
                _tenantConfiguration.Logger.Warning("Unable to remove task \"{taskId}\".", context.TaskId);
                return;
            }

            _tenantConfiguration.Logger.Warning("Removed task \"{taskId}\" from the context list.", context.TaskId);

            context.Dispose();
        }

        private IScheduledTask LoadInstance(string taskId)
        {
            var taskInfo = _taskProvider.GetTaskInfo(taskId);
            var factory = _taskProvider.GetTaskFactoryByTaskId(taskId);

            if (factory == null)
            {
                var message = string.Format("There was no factory containing the task definition \"{0}\""
                    , taskId);
                throw new InvalidOperationException(message);
            }

            var instance = factory.GetTask(taskInfo, _tenantConfiguration);
            if (instance == null)
            {
                var message = string.Format("There was instance returned from factory for task definition \"{0}\""
                    , taskId);
                throw new InvalidOperationException(message);
            }

            return instance;
        }

        private void WaitOnTasks(Task[] tasks)
        {
            if (!tasks.Any())
                return;

            try
            {
                Task.WaitAll(tasks);
            }
            catch (AggregateException ex)
            {
                _tenantConfiguration.Logger.Warning(ex, "Expected error occurred while waiting for \"{count}\" task(s) to complete.",
                    tasks.Length);
            }
            catch (Exception ex)
            {
                _tenantConfiguration.Logger.Error(ex, "Unknown error occurred while waiting for \"{count}\" task(s) to complete.",
                    tasks.Length);
            }
        }
    }
}