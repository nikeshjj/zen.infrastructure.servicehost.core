using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using StructureMap;
using StructureMap.Graph;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost
{
    public class ServiceManager : IDisposable
    {
        private readonly ITenantConfigurationInfo _tenantConfiguration;
        private readonly ITaskSchedule[] _schedules;
        private readonly IQueueRouterConfig[] _queueRouters;
        private readonly CancellationTokenSource _tokenSource;
        private readonly List<Task> _tasks;

        public ServiceManager(ITenantConfigurationInfo tenantConfiguration, ITaskSchedule[] schedules, IQueueRouterConfig[] queueRouters)
        {
            _tenantConfiguration = tenantConfiguration;
            _schedules = schedules;
            _queueRouters = queueRouters;
            _tokenSource = new CancellationTokenSource();
            _tasks = new List<Task>();
        }

        public void OnStart(string[] args)
        {
            _tenantConfiguration.Logger.Information("The console runner is starting.");

            var container = new Container(x =>
            {
                x.Scan(y =>
                {
                    y.AssembliesFromApplicationBaseDirectory(z => z.GetName().Name.StartsWith("Zen", StringComparison.Ordinal) ||
                                                            z.GetName().Name.EndsWith("AppService", StringComparison.Ordinal));
                    y.AddAllTypesOf<IScheduledTaskFactory>();
                    y.AddAllTypesOf<IQueueRouter>();
                });
            });

            var factories = container
                .GetAllInstances<IScheduledTaskFactory>()
                .ToArray();

            var subFactories = container
                .GetAllInstances<IQueueRouter>()
                .ToArray();

            var taskProvider = ServiceFactory.GetTaskProvider(factories);
            var scheduleProvider = ServiceFactory.GetScheduleProvider(_tenantConfiguration.Logger, _schedules);
            var activeContextProvider = ServiceFactory.GetActiveContextProviders();

            var controllerTask = new Task(() =>
            {
                _tenantConfiguration.Logger.Information("The controller task is starting.");

                var controller = new TaskController(_tenantConfiguration, scheduleProvider, activeContextProvider, taskProvider);
                controller.ExecuteThread(_tokenSource.Token);
            });
            controllerTask.ContinueWith(x =>
            {
                _tenantConfiguration.Logger.Information("The controller task has shutdown.");
                _tasks.Remove(x);
            });
            _tasks.Add(controllerTask);

            var subscriberProvider = ServiceFactory.GetSubscriberProvider(subFactories);
            var listenerProvider = ServiceFactory.GetListenerProvider(_tenantConfiguration, _tenantConfiguration.Logger, _queueRouters);

            var subscriberTask = new Task(() =>
            {
                _tenantConfiguration.Logger.Information("The subscriber task is starting.");
                var controller = new SubscriberController(_tenantConfiguration, listenerProvider, subscriberProvider);
                controller.ExecuteThread(_tokenSource.Token);
            });
            subscriberTask.ContinueWith(x =>
            {
                _tenantConfiguration.Logger.Information("The subscriber task has shutdown.");
                _tasks.Remove(x);
            });
            _tasks.Add(subscriberTask);

            controllerTask.Start();
            subscriberTask.Start();

            _tenantConfiguration.Logger.Information("The console runner has started.");
        }

        public void OnStop(int exitCode)
        {
            _tenantConfiguration.Logger.Information("The console runner is stopping. ExitCode - " +exitCode);

            _tokenSource.Cancel();
            Task.WaitAll(_tasks.ToArray());

            _tenantConfiguration.Logger.Information("The console runner has stopped. ExitCode - " + exitCode);
        }

        public void Dispose()
        {
            _tokenSource.Dispose();
        }
    }
}