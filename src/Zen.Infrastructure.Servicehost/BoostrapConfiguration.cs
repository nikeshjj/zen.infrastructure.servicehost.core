﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zen.Infrastructure.ServiceHost.Interfaces;
using Zen.Infrastructure.ServiceHost.Models;

namespace Zen.Infrastructure.ServiceHost
{
    public class BoostrapConfiguration : IBootstrapConfiguration
    {
        private readonly IList<ITaskSchedule> _schedules;
        private readonly IList<IQueueRouterConfig> _queueRouters;

        public BoostrapConfiguration()
        {
            _schedules = new List<ITaskSchedule>();
            _queueRouters = new List<IQueueRouterConfig>();
        }

        public ITaskSchedule[] Schedules
        {
            get { return _schedules.ToArray(); }
        }

        public IQueueRouterConfig[] QueueRouters
        {
            get { return _queueRouters.ToArray(); }
        }

        /// <summary>
        /// The schedule you would like to run your task
        /// uses the CronTab paradigm.
        /// </summary>
        /// <example>
        /// * * * * *
        /// - - - - -
        /// | | | | |
        /// | | | | +----- day of week (0 - 6) (Sunday=0)
        /// | | | +------- month (1 - 12)
        /// | | +--------- day of month (1 - 31)
        /// | +----------- hour (0 - 23)
        /// +------------- min (0 - 59)
        /// </example>
        /// <remarks>
        /// You can do some research on how to schedule your task via
        /// the documentation on the following links:
        /// 
        /// http://kvz.io/blog/2007/07/29/schedule-tasks-on-linux-using-crontab/ 
        /// </remarks>
        /// <param name="taskId">The task identifier</param>
        /// <param name="cronSchedule">See example for assistance</param>
        /// <param name="maxRetryAttempts">The max amount of retries</param>
        /// <returns></returns>
        public IBootstrapConfiguration AddScheduledTask(string taskId, string cronSchedule, int maxRetryAttempts)
        {
            _schedules.Add(TaskSchedule.Create(taskId, cronSchedule, maxRetryAttempts, null));
            return this;
        }

        public IBootstrapConfiguration AddScheduledTask(string taskId, string cronSchedule, int maxRetryAttempts,
            string timezoneName)
        {
            var timezoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timezoneName);
            _schedules.Add(TaskSchedule.Create(taskId, cronSchedule, maxRetryAttempts, timezoneInfo));
            return this;
        }

        /// <summary>
        /// The queue router that you would like to listen to.
        /// </summary>
        /// <param name="queueRouterId">The queue router identifier</param>
        /// <param name="queueName">The queue name</param>
        /// <returns></returns>
        public IBootstrapConfiguration AddQueueRouter(string queueRouterId, string queueName)
        {
            _queueRouters.Add(QueueRouterConfig.Create(queueRouterId, queueName));
            return this;
        }
    }
}