﻿using System;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost
{
    public class TenantConfiguration : ITenantConfiguration, ITenantConfigurationInfo
    {
        private readonly IConfigManager _manager;

        public TenantConfiguration()
        {
            _manager = new ConfigManager();
        }

        public ILogger Logger { get; set; }
        public string DbConnectionString { get; set; }
        public string EsbConnectionString { get; set; }

        public void SetLogger(ILogger logger)
        {
            Logger = logger;
        }

        public void SetDbConnectionString(string connectionString)
        {
            DbConnectionString = connectionString;
        }

        public void SetDbConnectionString(Func<IConfigManager, string> configManager)
        {
            if (configManager != null)
            {
                DbConnectionString = configManager(_manager);
            }
        }

        public void SetEsbConnectionString(string connectionString)
        {
            EsbConnectionString = connectionString;
        }

        public void SetEsbConnectionString(Func<IConfigManager, string> configManager)
        {
            if (configManager != null)
            {
                EsbConnectionString = configManager(_manager);
            }
        }
    }
}