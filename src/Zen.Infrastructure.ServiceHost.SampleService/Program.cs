﻿using System;

namespace Zen.Infrastructure.ServiceHost.SampleService
{
    static class Program
    {
        static void Main(string[] args)
        {
            Bootstrap.Main(args, x =>
            {
                x.SetLogger(new Logger("serilog", "http://Collector.zsys.io", "12202", "SampleService", "local"));
                x.SetDbConnectionString(_ => _.ConnectionString("DbConnectionString"));
                x.SetEsbConnectionString(_ => _.ConnectionString("EsbConnectionString"));
            }, x =>
            {
                //x.AddScheduledTask("mock.verylong.task", "* 12 * * *", 5, "Central Standard Time");
                x.AddScheduledTask("mock.longrunning.task", "* * * * *", 5);
                //x.AddScheduledTask("mock.queue.publish", "* * * * *", 5);
                //x.AddQueueRouter("mock.samplequeue.router", "servicehosttest");
            });
        }
    }
}
