using System.Threading;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.SampleService.Tasks
{
    public class VeryLongRunningScheduledTask : IScheduledTask
    {
        private readonly ILogger _logger;

        public VeryLongRunningScheduledTask(ILogger logger)
        {
            _logger = logger;
        }

        public void Execute(CancellationToken token)
        {
            _logger.Debug("Starting very long process");

            var i = 0;
            while (i < 12)
            {
                if (token.IsCancellationRequested)
                    return;

                i++;

                Thread.Sleep(10000);
                _logger.Debug("Sleeped for {0} seconds", i * 10);
            }

            _logger.Debug("Ended very long process");
        }
    }
}