using System.Threading;
using Zen.Infrastructure.Logging;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.SampleService.Tasks
{
    public class LongRunningScheduledTask : IScheduledTask
    {
        private readonly ILogger _logger;

        public LongRunningScheduledTask(ILogger logger)
        {
            _logger = logger;
        }

        public void Execute(CancellationToken token)
        {
            _logger.Information("5 seconds till the task can be canceled.");

            for (var i = 1; i <= 5; i++)
            {
                Thread.Sleep(1000);
                _logger.Information("{seconds} second(s) passed.", i);
            }

            _logger.Information("10 seconds till the task completes, you can cancel if you want.");

            for (var i = 1; i <= 10; i++)
            {
                if (token.IsCancellationRequested)
                    break;

                Thread.Sleep(1000);
                _logger.Information("{seconds} seconds passed.", i);
            }
        }
    }
}