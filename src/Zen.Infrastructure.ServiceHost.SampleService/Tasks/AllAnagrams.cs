using System.Collections.Generic;
using System.Threading;
using Zen.Infrastructure.Logging;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.SampleService.Tasks
{
    public class AllAnagramsScheduledTask : IScheduledTask
    {
        private readonly ILogger _logger;

        public AllAnagramsScheduledTask(ILogger logger)
        {
            _logger = logger;
        }

        public void Execute(CancellationToken token)
        {
            if (token.IsCancellationRequested)
                token.ThrowIfCancellationRequested();

            var anagrams = GetAllAnagrams("abba");
            foreach (var a in anagrams)
                _logger.Information(a);
        }
        
        public string[] GetAllAnagrams(string str)
        {
            var result = new List<string>();
            SetPer(str.ToCharArray(), result);
            return result.ToArray();
        }

        private static void Swap(ref char a, ref char b)
        {
            if(a==b)
                return;
            a ^= b;
            b ^= a;
            a ^= b;
        }

        public void SetPer(char[] list, List<string> array)
        {
            var x = list.Length - 1;
            Go(list, 0, x, array);
        }

        private static void Go(char[] list, int k, int m, ICollection<string> array)
        {
            int i;
            if (k == m)
            {
                var input = new string(list);
                if (!array.Contains(input))
                    array.Add(input);
            }
            else
                for (i = k; i <= m; i++)
                {
                    Swap(ref list[k],ref list[i]);
                    Go(list, k+1, m, array);
                    Swap(ref list[k],ref list[i]);
                }
        }
    }
}