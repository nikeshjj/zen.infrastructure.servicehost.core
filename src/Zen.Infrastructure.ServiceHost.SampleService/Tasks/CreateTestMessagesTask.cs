using System;
using System.Threading;
using Zen.Infrastructure.Logging;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.SampleService.Tasks
{
    public class CreateTestMessagesTask : IScheduledTask
    {
        private readonly ILogger _logger;
        private readonly IQueuePublisher _publisher;

        public CreateTestMessagesTask(ILogger logger, IQueuePublisher publisher)
        {
            _logger = logger;
            _publisher = publisher;
        }

        public void Execute(CancellationToken token)
        {
            for (var i = 0; i < 10; i++)
            {
                if (token.IsCancellationRequested)
                    token.ThrowIfCancellationRequested();

                Thread.Sleep(1000);

                var now = DateTime.Now;
                _logger.Information("Creating Test Message \"{0}\" at \"{1}\"", i, now);

                _publisher.Publish(new
                {
                    Id = i,
                    Message = "This is a test message",
                    DateTime = DateTime.Now
                });
            }
        }
    }
}