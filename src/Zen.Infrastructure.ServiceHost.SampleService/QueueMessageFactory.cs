using System;
using System.Linq;
using Zen.Infrastructure.ServiceHost.Interfaces;
using Zen.Infrastructure.ServiceHost.SampleService.Handlers;

namespace Zen.Infrastructure.ServiceHost.SampleService
{
    public class QueueRouter : IQueueRouter
    {
        private readonly string[] _queueRouterIdentifiers = {
            "mock.samplequeue.router"
        };

        public bool IsSatisfiedBy(IQueueRouterInfo info)
        {
            return _queueRouterIdentifiers.Contains(info.QueueRouterId, StringComparer.OrdinalIgnoreCase);
        }

        public IQueueMessageHandler GetHandler(IQueueMessageInfo info, ITenantConfigurationInfo context)
        {
            if (info.QueueRouterId.Equals("mock.samplequeue.router", StringComparison.OrdinalIgnoreCase))
            {
                return new MockQueueMessageHandler(context.Logger);
            }

            throw new NotImplementedException();
        }
    }
}