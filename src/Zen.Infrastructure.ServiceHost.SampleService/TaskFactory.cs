using System;
using System.Linq;
using Zen.Infrastructure.ServiceHost.Infrastructure.Queueing;
using Zen.Infrastructure.ServiceHost.Interfaces;
using Zen.Infrastructure.ServiceHost.SampleService.Tasks;

namespace Zen.Infrastructure.ServiceHost.SampleService
{
    public class ScheduledTaskFactory : IScheduledTaskFactory
    {
        private readonly string[] _taskIdentifiers = {
            "mock.longrunning.task",
            "mock.allanagrams.task",
            "mock.verylong.task",
            "mock.queue.publish"
        };

        public bool IsSatisfiedBy(ITaskInfo info)
        {
            return _taskIdentifiers.Contains(info.TaskId, StringComparer.OrdinalIgnoreCase);
        }

        public IScheduledTask GetTask(ITaskInfo info, ITenantConfigurationInfo context)
        {
            if (info.TaskId.Equals("mock.longrunning.task", StringComparison.OrdinalIgnoreCase))
            {
                return new LongRunningScheduledTask(context.Logger);
            }

            if (info.TaskId.Equals("mock.allanagrams.task", StringComparison.OrdinalIgnoreCase))
            {
                return new AllAnagramsScheduledTask(context.Logger);
            }

            if (info.TaskId.Equals("mock.verylong.task", StringComparison.OrdinalIgnoreCase))
            {
                return new VeryLongRunningScheduledTask(context.Logger);
            }

            if (info.TaskId.Equals("mock.queue.publish", StringComparison.OrdinalIgnoreCase))
            {
                var provider = QueueFactory.Create(context.EsbConnectionString);
                var publisher = provider.GetPublisher("servicehosttest");
                return new CreateTestMessagesTask(context.Logger, publisher);
            }

            throw new NotImplementedException();
        }
    }
}