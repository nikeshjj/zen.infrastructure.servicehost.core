using System;
using System.Threading;
using Zen.Infrastructure.Logging;
using Zen.Infrastructure.Logging.Interfaces;
using Zen.Infrastructure.ServiceHost.Interfaces;

namespace Zen.Infrastructure.ServiceHost.SampleService.Handlers
{
    public class MockQueueMessageHandler : IQueueMessageHandler
    {
        private readonly ILogger _logger;

        public MockQueueMessageHandler(ILogger logger)
        {
            _logger = logger;
        }

        public void Handle(IQueueMessage queueMessage, CancellationToken token)
        {
            var message = queueMessage.GetBody<TestMessage>();

            _logger.Information("Message {id} was submitted @ {datetime}."
                , message.Id
                , message.DateTime);
        }
    }

    public class TestMessage
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
    }
}